//  ============================================================================
2015.09.20
- Added panels resize following the # of editors in them
2015.09.26
- Corrected a bug in panel maximizing
- Better panel sizing
